<?php

/**
 * @file
 * Contains \Drupal\language\LanguageServiceProvider.
 */

namespace Drupal\edprofiler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Overrides the language_manager service to point to language's module one.
 */
class EdprofilerServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $def = 0;
    foreach (Settings::get('edprofiler.subscribers', array()) as $class => $n) {
      for ($i = 0; $i < $n; $i++) {
        $container->register('edprofiler.subscriber_' . $def, $class)
          ->addTag('event_subscriber');
        $def++;
      }
    }
  }

}
