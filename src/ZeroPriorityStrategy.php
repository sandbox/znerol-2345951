<?php

namespace Drupal\edprofiler;

class ZeroPriorityStrategy {
  public function getPriority($event_name) {
    return 0;
  }
}
