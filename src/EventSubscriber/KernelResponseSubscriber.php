<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelResponseSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::RESPONSE;
}
