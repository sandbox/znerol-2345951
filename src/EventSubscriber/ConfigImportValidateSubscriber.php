<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigImportValidateSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::IMPORT_VALIDATE;
}
