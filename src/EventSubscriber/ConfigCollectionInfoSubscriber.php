<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigCollectionInfoSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::COLLECTION_INFO;
}
