<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Routing\RoutingEvents;

class RoutingAlterSubscriber extends SubscriberBase {
  static protected $eventName = RoutingEvents::ALTER;
}
