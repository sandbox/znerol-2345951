<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\block\Event\BlockEvents;

class BlockConditionContextSubscriber extends SubscriberBase {
  static protected $eventName = BlockEvents::CONDITION_CONTEXT;
}
