<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelRequestSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::REQUEST;
}
