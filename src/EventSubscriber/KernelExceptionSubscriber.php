<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelExceptionSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::EXCEPTION;
}
