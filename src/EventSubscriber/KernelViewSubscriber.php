<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelViewSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::VIEW;
}
