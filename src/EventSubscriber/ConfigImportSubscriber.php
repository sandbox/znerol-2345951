<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigImportSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::IMPORT;
}
