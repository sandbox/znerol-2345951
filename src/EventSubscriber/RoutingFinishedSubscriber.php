<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Routing\RoutingEvents;

class RoutingFinishedSubscriber extends SubscriberBase {
  static protected $eventName = RoutingEvents::FINISHED;
}
