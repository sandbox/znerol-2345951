<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Routing\RoutingEvents;

class NoopSubscriber extends SubscriberBase {
  static protected $eventName = 'edprofiler.noop';
}
