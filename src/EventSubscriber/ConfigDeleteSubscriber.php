<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigDeleteSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::DELETE;
}
