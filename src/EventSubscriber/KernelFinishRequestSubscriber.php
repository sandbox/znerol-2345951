<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelFinishRequestSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::FINISH_REQUEST;
}
