<?php

namespace Drupal\edprofiler\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;

class KernelControllerSubscriber extends SubscriberBase {
  static protected $eventName = KernelEvents::CONTROLLER;
}
