<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigSaveSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::SAVE;
}
