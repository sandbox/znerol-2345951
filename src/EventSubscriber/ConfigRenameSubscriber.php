<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;

class ConfigRenameSubscriber extends SubscriberBase {
  static protected $eventName = ConfigEvents::RENAME;
}
