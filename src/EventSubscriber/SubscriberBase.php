<?php

namespace Drupal\edprofiler\EventSubscriber;

use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SubscriberBase implements EventSubscriberInterface {

  static protected $eventName = 'no.event';
  static protected $priorityStrategy;

  public function noop() {
  }

  public static function getSubscribedEvents() {
    return [
      static::$eventName => array('noop', static::getPriority(static::$eventName)),
    ];
  }

  protected static function getPriority($eventName) {
    if (!isset(static::$priorityStrategy)) {
      $class = Settings::get('edprofiler.priority_strategy', '\Drupal\edprofiler\ZeroPriorityStrategy');
      static::$priorityStrategy = new $class();
    }

    return static::$priorityStrategy->getPriority($eventName);
  }
}
