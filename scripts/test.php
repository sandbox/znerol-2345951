<?php

/**
 * @file
 * Event dispatcher profiler CLI.
 *
 * Installation:
 * * Copy the script test.php to your drupal root.
 * * Edit settings.php and add the following line:
 *     $settings['edprofiler.subscribers']['Drupal\edprofiler\EventSubscriber\NoopSubscriber'] = 8;
 *   This will add the noop-subscribers 8 times to the container.
 *
 * Usage:
 * * Clear caches
 *     drush cr
 * * Run script once to rebuild the container:
 *     php test.php
 * * Run script 50 times to gather the data:
 *     for ((i=0;i<50;i++)); do php test.php; done
 */

use Composer\EventDispatcher\Event;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;

$eventName = 'edprofiler.noop';

$autoloader = require_once 'autoload.php';

$request = Request::createFromGlobals();
$kernel = DrupalKernel::createFromRequest($request, $autoloader, 'prod');
$kernel->boot();
$kernel->preHandle($request);

$container = $kernel->getContainer();

// Profile instantiation of event_manager.
assert($container->initialized('event_dispatcher') === FALSE);
$t0 = microtime(TRUE);
$event_dispatcher = $container->get('event_dispatcher');
$t1 = microtime(TRUE);
assert($container->initialized('event_dispatcher') === TRUE);
$t_init = $t1 - $t0;

// Profile initial dispatch.
$t0 = microtime(TRUE);
$event_dispatcher->dispatch(new Event($eventName), $eventName);
$t1 = microtime(TRUE);
$t_d1 = $t1 - $t0;

// Profile subsequent dispatch.
$t0 = microtime(TRUE);
$event_dispatcher->dispatch(new Event($eventName), $eventName);
$t1 = microtime(TRUE);
$t_d2 = $t1 - $t0;

// Gather data.
$n = count($event_dispatcher->getListeners($eventName));
$m = array_reduce($event_dispatcher->getListeners(), function($carry, $callables) {
  return $carry + count($callables);
});

printf("%d,%d,%f,%f,%f\n", $n, $m, $t_init * 1000, $t_d1 * 1000, $t_d2 * 1000);
